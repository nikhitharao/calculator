//#include<stdio.h>

int addition(int op1, int op2)
{
	int res = op1+op2;
	printf("Result of addition=%d", res);
}

int subtraction(int op1, int op2)
{
	int res = op1-op2;
	printf("Result of sub=%d", res);
}

int multi(int op1, int op2)
{
	int res = op1*op2;
	printf("Result of mul=%d", res);
}

int division(int op1, int op2)
{
	while(op2==0)
	{
		printf("Divide by zero error");
		exit(0);
	}
	int res = op1/op2;
	printf("Result of division=%d", res);
}