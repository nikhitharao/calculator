#include"model.c"
#include"view.c"
#include<stdio.h>

enum operator{ add=1, sub, mul, divi};

int main()
{
	enum operator op;

	int num1 = getNum1();
	int num2 = getNum2();
	
	do{
	op=choice(); 
	switch(op)
	{
	case add: 
 		addition(num1, num2);
  		break;
	case sub:
		subtraction(num1, num2);
		break;
	case mul:
		multi(num1, num2);
		break;
	case divi:
		division(num1, num2);
		break;
	default:
		printf("Default\n");
		exit(0);
	}
	}while(op<5);
}